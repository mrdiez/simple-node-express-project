
# How to setup a simple Express Node Typescript project with automated tests:

## Use this project

* Install dependencies: npm i

* Build then run the project:
    - With automated compiler: npm run dev
    - Without automated compiler: npm run start

* Open your browser at: http://127.0.0.1:3000/

## New project init

* Install Node: https://nodejs.org/en/download/

* Create your project folder:
    - mkdir my_project_name
    - cd my_project_name

* Init your project: npm init -y

* Install Node type definitions: npm i --save @types/node

* Install Express: npm i --save express @types/express

* Create an src folder and an app file like ./src/app.ts then add some code:
```
import express from 'express';

const app = express();
app.get('*', (req, res) => res.status(200).json({ message: 'Hello World!' }));

export default app;
```

* Create an index file like ./src/index.ts then add some code:
```
import app from './app';

const hostname = '127.0.0.1';
const port = 3000;
app.listen(port, hostname, () => console.log(`Server running at http://${hostname}:${port}/`));
```

## Typescript init

* Install Typescript and recommended settings: npm i --save-dev typescript @tsconfig/recommended

* Init Typescript configuration file: npx tsc --init

* Add recommended settings to the Typescript configuration file ./tsconfig.json:
```
{
    "extends": "@tsconfig/recommended/tsconfig.json",
    "compilerOptions": {
        ...
    }
}
```

* Customize the Typescript configuration file ./tsconfig.json with your own rules:
```
{
    "outDir": "./dist",
    ...
    "compilerOptions": {
        ...
    }
}
```

* Install Typescript automated compiler: npm i --save-dev ts-node-dev

* Add a new script to ./package.json in order to run compiler:
```
{
    ...
    "scripts": {
        "build": "tsc -p tsconfig.json",
        "start": "npm run build && node ./dist/index.js",
        "dev": "ts-node-dev --respawn --pretty --transpile-only src/index.ts",
        ...
    },
    ...
}
```

## Linter init

* Install Typescript linter: npm install --save-dev eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin

* Init EsLint configuration file: npx eslint --init

* Answer to initializer questions:
    - How would you like to use ESLint? To check syntax and find problems
    - What type of modules does your project use? JavaScript modules (import/export)
    - Which framework does your project use? None of these
    - Does your project use TypeScript? Yes
    - Where does your code run? Node
    - What format do you want your config file to be in? JSON
    - Would you like to install them now with npm? Yes

* Customize the EsLint configuration file ./.eslintrc.json with your own rules:
```
{
    ...
    "rules": {
        "@typescript-eslint/quotes": [
            "error",
            "single",
            {
            "avoidEscape": true,
            "allowTemplateLiterals": true
            }
        ],
        ...
    }
}
```

* Create an ./.eslintignore file with at least:
```
node_modules
dist
```

* Add a new script to ./package.json in order to run linter:
```
{
    ...
    "scripts": {
        "lint": "eslint src/ --ext .js,.jsx,.ts,.tsx",
        ...
    },
    ...
}
```

* Install EsLint IDE extension

* If you set the above "single quote" rule then:
    - Try to set double quote to a string in your ./src/index.html
    - Ensure that you get an error without having to run any command

## Tests init

* Install Jest, Babel and SuperTest: npm i --save-dev jest @types/jest ts-jest babel-jest @babel/preset-env supertest @types/supertest

* Init Jest configuration file: npx jest --init

* Answer to initializer questions:
    - Would you like to use Jest when running "test" script in "package.json"? Yes
    - Would you like to use Typescript for the configuration file? Yes
    - Choose the test environment that will be used for testing: Node
    - Do you want Jest to add coverage reports? Yes
    - Which provider should be used to instrument code for coverage? Babel
    - Automatically clear mock calls, instances and results before every test? Yes

* Add some rules to the Jest configuration file ./jest.config.ts in order to add Typescript compatibilty :
```
export default {
    ...
    preset: 'ts-jest',
    transform: {
        '^.+\\.(ts|tsx)?$': 'ts-jest',
        '^.+\\.(js|jsx)$': 'babel-jest',
    },
}
```

* Create a Babel configuration file ./babel.config.js then add some code:
```
export const presets = ['@babel/preset-env'];
```

* Add a new script to ./package.json if initializer not already did it:
```
{
    ...
    "scripts": {
        "test": "jest",
        ...
    },
    ...
}
```

* Create a simple test file like ./src/app.test.ts then add some code:
```
import { Server, createServer } from 'http';
import { SuperAgentTest, agent as supertest } from 'supertest';
import app from './app';

describe('Server start test', () => {
  let server: Server;
  let request: SuperAgentTest;

  beforeAll((done) => {
    server = createServer(app);
    server.listen(done);
    request = supertest(server);
  });

  afterAll((done) => {
    server.close(done);
  });

  it('Ensure server response status is 200', async () => {
    const response = await request.get('/');
    expect(response.status).toBe(200);
  });

  it('Ensure server response text is "Hello World!"', async () => {
    const response = await request.get('/');
    expect(response.body.message).toBe('Hello World!');
  });
});
```

* Run automated tests: npm run test

## Git init

* Create a dedicated project on Gitlab, Github or whatever you want

* Initialize your directory as a Git project (initial branch is master by default): git init --initial-branch=main

* Create a .gitignore file with at least:
```
dist
node_modules
```

* Add this repository as remote origin for your project: git remote add origin git@gitlab.com:mrdiez/simple-node-express-project.git

* Then push your project:
    - git add .
    - git commit -m "initial commit"
    - git push -u origin main
