import { Server, createServer } from 'http';
import { SuperAgentTest, agent as supertest } from 'supertest';
import app from './app';

describe('Server start test', () => {
  let server: Server;
  let request: SuperAgentTest;

  beforeAll((done) => {
    server = createServer(app);
    server.listen(done);
    request = supertest(server);
  });

  afterAll((done) => {
    server.close(done);
  });

  it('Ensure server response status is 200', async () => {
    const response = await request.get('/');
    expect(response.status).toBe(200);
  });

  it('Ensure server response text is "Hello World!"', async () => {
    const response = await request.get('/');
    expect(response.body.message).toBe('Hello World!');
  });
});